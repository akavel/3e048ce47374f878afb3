@echo off
:: This file (bootstrap.bat) is a helper for building "luvit.exe" and "lit.exe"
:: with pre-built "luvi.exe".


:: We require luvi.exe.
:: TODO: find out a way to auto-download it on "bare" Windows?
if not exist luvi.exe (
	echo error: luvi.exe missing!
	echo Please download luvi-*.exe from https://github.com/luvit/luvi/releases, then copy it here and rename to "luvi.exe".
	exit /b 1
)

:: Download lit.git
:: TODO: is the 2>nul needed here?
rmdir /q/s lit.git 2>nul
:: TODO: do shallow clone for speed?
git clone https://github.com/luvit/lit lit.git
if errorlevel 1 (
	echo error: could not git clone lit! maybe git is not installed?
	exit /b 1
)

:: Build lit.exe
luvi lit.git -o ../lit.exe
if errorlevel 1 (
	echo error: could not build lit.exe!
	exit /b 1
)

:: Download luvit.git
:: TODO: is the 2>nul needed here?
rmdir /q/s luvit.git 2>nul
:: TODO: do shallow clone for speed?
git clone https://github.com/luvit/luvit luvit.git
if errorlevel 1 (
	echo error: could not git clone luvit! maybe git is not installed?
	exit /b 1
)

:: Build luvit.exe
:: TODO: or, `lit.exe make`? what's the difference?
luvi luvit.git -o ../luvit.exe
if errorlevel 1 (
	echo error: could not build luvit.exe!
	exit /b 1
)

